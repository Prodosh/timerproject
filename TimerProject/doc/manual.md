# INFT2063 - Assignment 1 Timer Application

![image Main Screen](./manualPics/mainScreen.png)


Welcome to our Timer Application project! This manual details how to use the program for anyone who is having any difficulty with understanding
how any of the components work.

*IN ORDER TO BOOT UP THE PROGRAM, SIMPLY RUN THE .JAVA FILE 'TIMERSCREEN', which should run by default if you're running the program from your IDE and click*
*on the Run button. You should be greeted with the above window!*

You can use this program for the following features:

  - 1. Create times, set names, and queue them
  - 2. Play through the set tasks, displaying the time remaining
  - 3. Rearrange tasks freely
  - 4. Import and export tasks as .csv files
  - 5. View a log of all completed tasks
  
  
# 1. Create and Set Tasks  
  
![image Add a new task!](./manualPics/newTask.png) 
  
To create and add a new task, simply enter a name for your task into the box highlighted in the above image, and then enter a time in minutes and seconds
for your task by clicking the arrows beside the numbered boxes. Once you are satisfied with your tasks name and duration, click on the green button labelled
"ADD", and you will see your task appear in the queue above, such as in the image below.

![image Task added!](./manualPics/taskAdded.png) 
  
  
# 2. Play Through the Set Tasks
  
![image Let the timer go!](./manualPics/pressGo.png) 
  
Once you've added your tasks to the schedule and are happy, you can press the button highlighted in the above image, labelled "Play" to begin the countdown.
You'll see the timer begin to decrease, and the current task will be shown on screen. Once the timer is finished, the program will move onto the next
task, and once all are complete, the program will display a message to let you know.

![image The timer is done!](./manualPics/timerDone.png) 
  

# 3. Rearrange Tasks Freely  
  
![image Select a task!](./manualPics/grabTask.png) 
  
Rearranging tasks is a snap! All you have to do is select a task by clicking on it once. When you do, you'll see the task is now highlighted in blue. If you
want to select multiple tasks to move at once, hold down your Shift key and click on more tasks to select them as well. Once you have the tasks you want to
shift, simply click and drag them to your desired position in the list, and the tasks will rearrange themselves.

![image You moved the task!](./manualPics/taskMoved.png)  

# 4. Import and Export Tasks as .csv Files  
  
![image Select a task!](./manualPics/grabTask.png) 
  
To select tasks you want to export, and simply drag them out of the queue and onto the place you want to export them to. The program will 
automatically export the tasks as a .csv file for you! You can do this one with task at a time, or as many as you want!

![image You exported the task!](./manualPics/taskExported.png) 

You can import tasks just as easily. Simply click and drag a .csv file containing tasks into the queue, and they'll be automatically imported!

![image Drag in the task!](./manualPics/dragTaskIn.png)

![image You imported the task!](./manualPics/taskImported.png)

# 5. View a log of all completed tasks
  
![image Summon the log!](./manualPics/summonLog.png) 
  
Finally, you can click on the button highlighted in the image to display a log to the screen! From the log, you can see all tasks that have been
completed since you opened the program, and they'll stay there even if you close the log and then reopen it, so you don't have to worry about losing
track of completed tasks until you fully shut down the program. 
Furthermore, if you want to re-add a task from the log back into the main queue, you can just double click on it, and it'll re-add itself back to the queue.

![image Return a task!](./manualPics/readdTask.png) 
  
  
==========================================================================================================================

That concludes the manual! If you have any further questions, please email me at [frapr003@mymail.unisa.edu.au](frapr003@mymail.unisa.edu.au)!