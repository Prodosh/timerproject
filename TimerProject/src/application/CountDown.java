package application;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CountDown {
	
	// The actual timer
	private Timer timer;
	
	// Interval at which a SubTasks time properties (m_min, m_sec) will the changed, 1000 milliseconds in this case
	private int refreshInterval = 1000;
	
	// Queue of SubTasks
	private Queue<SubTask> m_tasks;
	
	// current SubTask time that will be displayed to the screen
	public StringProperty timeString;
	
	// current SubTask title that will be displayed to the screen
	public StringProperty taskTitleString;
	
	// Will temporarily hold a recently finished task
	private SubTask recent;
	
	public BooleanProperty taskFinished;
	

	


	public CountDown() 
	{
		
		m_tasks = new LinkedList<SubTask>();
		timeString = new SimpleStringProperty();
		taskTitleString = new SimpleStringProperty();
		
		taskFinished = new SimpleBooleanProperty(false);
		

		


		setTimeString();
		setTitleString();
	}
	
	// Add a SubTask to the queue
	public void addTask(SubTask s)
	{
		m_tasks.add(s);
	}
	
	private void toggleTaskFinished()
	{
		if(taskFinished.getValue())
			taskFinished.set(false);
		else
			taskFinished.set(true);
		
	}
	


	

	// Get the SubTask list
	public Queue<SubTask> getTasks() { return m_tasks; }
	
	// Starts the actual countdown
	public void startCountDown()
	{
		// If there are no tasks do nothing
		if(m_tasks.isEmpty())
			return;
		
		// Make sure multiple timers are not created
		if(!m_tasks.peek().isRunning())
		{
			timer = new Timer();
		
			
			
			// Start the timer with a new TimerTask
			// TimerTask objects take in an entire function as their first parameter
			// The second parameter is a delay before the timer starts, this is set to 0 since we want the timer to run immediatly
			// after we tell it to.
			// The third parameter is our refreshInterval member variable which is the interval the run function is called at
			timer.scheduleAtFixedRate(new TimerTask() {
				
				// Is called at every refreshInterval (1000 milliseconds)
				public void run()
				{
					
					// If the current task is not running...
					//System.out.println(m_tasks.peek().getTitle());
					if(!m_tasks.peek().isRunning())
					{
						// Set it to running
						m_tasks.peek().setRunning(true);
						
						// Set the title of task to be displayed
						
						Platform.runLater(() -> setTitleString());
						//System.out.println(m_tasks.peek().getTitle());
					}
					
					
					//m_tasks.peek().printTime();
					
					// Set the time to be displayed
					setTimeString();
					
					// If the time can't decrement the m_sec variable in a SubTask (eg. when its time has run out)...
					if(!decrementSec())
					{
						// remove the SubTask from the Queue
						m_tasks.peek().setFinished();
						recent = m_tasks.poll();
						
						// Signal the list view on the GUI that the task has finished
						toggleTaskFinished();
						

					


						// If the Queue is empty now stop the timer
						if(m_tasks.isEmpty())
						{
							Platform.runLater(() -> taskTitleString.set("All tasks done"));
							timer.cancel();
							timer = null;
						}
					}
				}
				
			}, 0, refreshInterval);		
		}

	}
	
	// Stop timer method is currently called when the window is closed, we don't want the Timer to be running in the background
	// if the user closes the window randomly during a SubTask. If you are unsure if your code is still running after the window is
	// closed there is a .printTime() statement above that is commented and just uncomment that which will display the time to the
	// console.
	// You will want to call this explicitly later if you allow the user the reschedule tasks while they are running SubTasks
	public void stopTimer()
	{
		if(!m_tasks.isEmpty())
		{
			if(timer != null)
			{
				m_tasks.peek().setRunning(false);
				timer.cancel();
				timer = null;
			}
		}
	}
	
	public boolean timerRunning()
	{
		if(timer == null)
			return false;
		
		return true;
	}
	
	public void resetCurrentTask()
	{
		if(!m_tasks.isEmpty())
		{
			if(timerRunning())
			{
				stopTimer();
				m_tasks.peek().reset();
				startCountDown();
			}
			else
			{
				m_tasks.peek().reset();
				setTimeString();
			}
		}
	}
	
	public SubTask getRecent() { return recent; }
	
	// Takes an ArrayList of id's of the subtasks that will be re-ordered
	// Also takes in the id of the subtask that will either be push up or down
	public void reOrderTasks(ArrayList<Integer> idList, int orderID)
	{
		
		LinkedList<SubTask> oldList = (LinkedList<SubTask>) m_tasks;
		Queue<SubTask> newList = new LinkedList<SubTask>();
		
		// Get the index of the first and last sub task in the idList.
		int smallestIndex = indexOf(idList.get(0));
		int largestIndex = indexOf(idList.get(idList.size()-1));
		
		// The index of the subtask pane that will either be pushed up or down by the selected tasks
		int indexOfOrder = indexOf(orderID);
		
		// If indexes are invalid then end the function
		if(smallestIndex == -1 || largestIndex == -1 || indexOfOrder == -1)
			return;
		
		// Loop through the queue of subtasks
		for(int i = 0; i < oldList.size(); ++i)
		{
			// If we are on the task that user drag and dropped on
			if(i == indexOfOrder)
			{
				// If the index of the first subtask in the idList array is smaller than indexOfOrder
				// i.e. the user has dragged something from the bottom of the list to put at the top
				if(indexOfOrder < smallestIndex)
				{
					// Add all the subtasks in that have the id's in idList into the newList
					for(int j = smallestIndex; j <= largestIndex; ++j)
					{
						newList.add(oldList.get(j));
					}
					
					// Then add the subtask at the current index into the new list
					newList.add(oldList.get(i));
				}
				// else if the indexOfOrder is greater than the index of the subtask whose id is the last element of idList
				// i.e. the user has dragged something from the top to the bottom
				else if(indexOfOrder > largestIndex)
				{
					// Add the subtask at the current index in
					newList.add(oldList.get(i));
					
					// Then add all subtasks from the smallest index to the largest index
					for(int j = smallestIndex; j <= largestIndex; ++j)
					{
						newList.add(oldList.get(j));
					}
					
				}
			}
			// If the index of the current SubTask is not in between the smallest and largestIndex than add them in normally
			else if(i < smallestIndex || i > largestIndex)
			{
				newList.add(oldList.get(i));
			}
		}
		
		// Assign the newLIst of reordered tasks to the old queue
		m_tasks = newList;
	}
	
	// Finds the index of a subtask with a passed in id parameter
	// return -1 if not found
	private int indexOf(int id)
	{
		LinkedList<SubTask> newList = (LinkedList<SubTask>) m_tasks;
		
		for(int i = 0; i < m_tasks.size(); ++i)
		{
			if(newList.get(i).getId() == id)
				return i;
		}
		
		return -1;
	}
	
	// Sets what the time display should look like
	private void setTimeString()
	{
		if(m_tasks.isEmpty())
		{
			timeString.setValue("00:00");
			return;
		}
		// All of this is pretty much just formatting except for the last line.
		String minute = Integer.toString(m_tasks.peek().getMin());
		String second = Integer.toString(m_tasks.peek().getSec());
		
		String temp = "0";
		
		if(m_tasks.peek().getMin() < 10)
		{
			temp += minute;
			minute = temp;
			temp = "0";
		}
		
		if(m_tasks.peek().getSec() < 10)
		{
			temp += second;
			second = temp;
			temp = "0";
		}
		final String displayMin = minute;
		final String displaySec = second;
		
		
		// When a timer is running javafx doesn't actually allow you to update things on the GUI the normal way.
		// So when ever the user could possibly doing things that could affect the look of
		// the GUI (eg. adding tasks, pausing tasks, deleting tasks) we have to use this weird syntax.
		// Platform.runLater() basically tells java that we want to call a function in this case (timeString.setValue()) so it
		// should put it somewhere in its Event Queue. The function will then be called in-between updates to the display.
		Platform.runLater(() -> timeString.setValue(displayMin + ":" + displaySec));
	}
	
	// Sets what the title display should look like
	private void setTitleString()
	{
		//System.out.println("In function setTitle");
		
		if(m_tasks.isEmpty())
		{
			taskTitleString.setValue("Press Play");
			return;
		}
		
		
		taskTitleString.setValue(m_tasks.peek().getTitle());
	}
	
	// Decrements the minutes of the SubTask at the top of the queue, please don't alter this logic
	private boolean decrementMin()
	{
		if(m_tasks.peek().getMin() == 0)
			return false;
		
		m_tasks.peek().setMin(m_tasks.peek().getMin()-1);
		
		return true;
		
	}
	
	// Decrements the seconds of the SubTask at the top of the queue, please don't alter this logic
	private boolean decrementSec()
	{
		if(m_tasks.peek().getSec() == 0)
		{
			if(decrementMin())
			{
				m_tasks.peek().setSec(59);
				return true;
			}
			else
				return false;
		}
		
		m_tasks.peek().setSec(m_tasks.peek().getSec()-1);
		
		return true;
	}
	
	// Empties the current list of tasks
	public void clearTasks()
	{
		this.m_tasks.clear();
	}

}
