package application;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Queue;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javafx.collections.ObservableList;
import javafx.scene.control.ListView;

public class CsvHandler {
	


	// Generic constructor
	public CsvHandler() {
		
	};
	
	// Reads from a CSV file and adds all tasks within into the passed CountDown object
	public void assignTasks(CountDown cd, File file, TaskView list) {
		
		if(file == null)
			return;
		
		// Create a string we can use to store the current line of a CSV file
		String currentLine;
		
		// An array to hold values of the line when split into data
		String[] values;
		
		// Subtask variable that will hold each new task as we make them
		SubTask newTask = null;
		
		// Try catch block for trying to connect our Scanner to the file. 
		// Catch in case the file doesn't exist or something else goes wrong.
		try {
			Scanner inputStream = new Scanner(file);
			
			// Until we reach the end of the CSV file;
			while (inputStream.hasNext()) {
				
				// Assign the next line to a string
				currentLine = inputStream.nextLine();
				
				// Split the read line at commas, placing them, inside the values array
				values = currentLine.split(",");
				
				// Attempt assign newTask a new SubTask object, passing it the name, and then the time values from the current line
				try {
					if(Integer.parseInt(values[1]) >= 0 && Integer.parseInt(values[1]) <= 59 && Integer.parseInt(values[2]) >= 0 && Integer.parseInt(values[2]) <= 59 )
						newTask = SubTask.createTask(values[0],  Integer.parseInt(values[1]), Integer.parseInt(values[2]));
					else
						newTask = SubTask.createTask(values[0],  0, 10);

					
				} catch (Exception e) {
					// On a failed attempt to load a task, usually caused by incorrect syntax in the CSV file, generate and add 
					// a task using a default time of 10 seconds. Assigning a name from the list SHOULD always be fine because 
					// regardless what is in the file, it should be assigned to the values array as a string.
					
					// It should also not matter at all if there are an incorrect amount of values in the CSV file, because if
					// there are, they'll be assigned to later indexes in the values array, which won't impede createTask() from
					// accessing the values at index 0, 1 and 2.
					newTask = SubTask.createTask(values[0],  0, 10);
					
				}
				
				// Add the new task to the passed in CountDown object
				if(newTask != null)
				{
					list.addTask(new SubTaskPane(newTask));
					cd.addTask(newTask);
				}

				
			}
			
			// Close the stream to end
			inputStream.close();
		} 
		
		// Unlikely but in case something breaks;
		catch (FileNotFoundException e) {
			
		}
	};

	public void exportTask(SubTask task, File file) {
		
		if(file == null)
			return;
		
		try {
			FileWriter myWriter = new FileWriter(file, true);
			myWriter.append(task.getTitle() + "," + task.getSaveMin() + "," + task.getSaveSec() + "\n");
			
			myWriter.close();
		} 
		
		catch (Exception e) {
			
		}
		
	}
	
	public void exportTask(Queue<SubTask> tasks, File file) {
		
		if(file == null)
			return;
		
		try {
			PrintWriter myWriter = new PrintWriter(file);
			
			for (SubTask s : tasks)
				myWriter.append(s.getTitle() + "," + s.getSaveMin() + "," + s.getSaveSec() + "\n");
			
			myWriter.close();
		} 
		
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	

}
