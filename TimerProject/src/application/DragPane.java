package application;

import java.util.ArrayList;


import javafx.scene.SnapshotParameters;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public class DragPane 
{
	
	private Pane draggable;
	private GridPane taskGrid;
	
	public DragPane()
	{
		
	}
	
	public DragPane(ArrayList<SubTaskPane> s)
	{
		// Set up the pane
		draggable = new Pane();
		taskGrid = new GridPane();
		taskGrid.setVgap(10);
		
		draggable.setStyle("-fx-background-color: rgb(112, 46, 54)");
		
		draggable.setMaxHeight(s.size() * 35 + s.size() * 10);
		
		// Add the the subtask panes passed in to a gridpane
		for(int i = 0; i < s.size(); ++i)
		{
			SubTaskPane sPane = new SubTaskPane(s.get(i).getTask());
			sPane.setMinWidth(473);
			sPane.setDetectEvents(false);
			taskGrid.add(sPane, 0, i);
		}
		
		// add the grid pane to the  draggable pane
		draggable.getChildren().add(taskGrid);
		
		
		
	}
	
	public Pane getDraggable() { return draggable; }
	

	// Return an image of the draggable Pane
	public WritableImage getSnapShot()
	{
		SnapshotParameters sp = new SnapshotParameters();
		
		sp.setFill(Color.LIGHTBLUE);
		
		WritableImage snapShot = draggable.snapshot(sp, null);
		
		
		
		return snapShot;
	}


}


