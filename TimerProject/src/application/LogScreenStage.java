package application;

import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class LogScreenStage {
	
	Stage stage;
	Scene scene;
	private BorderPane content;
	
	public LogScreenStage(double sizeX, double sizeY, double positionX, double positionY, ListView<SubTask> log) {
		
		stage = new Stage();
		stage.setX(positionX);
		stage.setY(positionY);
		
		content = new BorderPane();
		log.setPrefHeight(stage.getHeight());
		log.setPrefWidth(stage.getWidth());
		content.setCenter(log);
		
		
		scene = new Scene(content, sizeX, sizeY);
		stage.setScene(scene);
		stage.setTitle("Log");
		//stage.show();
	}

}
