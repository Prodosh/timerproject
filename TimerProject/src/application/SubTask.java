package application;

import java.time.format.DateTimeFormatter;

public class SubTask implements java.lang.Comparable
{
	// Id to identify specific tasks
	private int m_id;
	private int m_saveMin;
	private int m_saveSec;
	private String m_timeFinished;
	
	// Title, minuts, seconds and a bool thats says if the task is running or not
	private String m_title;
	private int m_min = 0;
	private int m_sec = 0;
	private boolean m_running = false;
	
	private static int staticId = 0;
	
	public SubTask() {
		// TODO Auto-generated constructor stub
	}
	
	public SubTask(int id, String title, int min, int sec)
	{
		m_id = id;
		m_title = title;
		m_min = min;
		m_sec = sec;
		m_saveMin = min;
		m_saveSec = sec;
	}

	// Getters
	public String getTitle() { return m_title; }
	public int getMin() { return m_min; }
	public int getSec() { return m_sec; }
	public int getSaveMin() { return m_saveMin; }
	public int getSaveSec() { return m_saveSec; }
	public int getId() { return m_id; }
	public boolean isRunning() { return m_running; }
	public String getFinished() {return m_timeFinished; }
	
	// Setters
	public void setTitle(String title) { m_title = title; }
	public void setMin(int min) { m_min = min; }
	public void setSec(int sec) { m_sec = sec; }
	public void setRunning(boolean running) { m_running = running; }
	public void setFinished() { m_timeFinished = new String(java.time.LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm")).toString()); }
	public void setID(int id) { m_id = id; }
	
	// Static createTask function that is the preffered way of creating SubTasks. You don't have to worry about the id. Use this
	// instead of the constructor directly
	public static SubTask createTask(String title, int minutes, int seconds)
	{
		return new SubTask(++staticId, title, minutes, seconds);
	}
	
	// Print the SubTask's current minute and second to the console
	public void printTime() 
	{
		System.out.println(m_min + ":" + m_sec); 
	}
	
	public String getWholeTime()
	{
		String min = "0";
		
		if(m_saveMin < 10)
			min += Integer.toString(m_saveMin);
		else
			min = Integer.toString(m_saveMin);
		
		String sec = "0";
		
		if(m_saveSec < 10)
			sec += Integer.toString(m_saveSec);
		else
			sec = Integer.toString(m_saveSec);
		
		return min + ":" + sec;
		
	}
	
	public String getCsvFormat()
	{
		return m_title + "," + Integer.toString(m_saveMin) + "," + Integer.toString(m_saveSec);
	}
	
	public void reset()
	{
		m_min = m_saveMin;
		m_sec = m_saveSec;
	}
	
	// Basic compareTo
	public int compareTo(Object o)
	{
		if (this == o)
			return 1;
		
		if(o instanceof SubTask)
		{
			SubTask t = (SubTask) o;
			
			if(m_id != t.getId())
				return 0;
			else
				return 1;
		}
		else
			return 0;
	}

	public String toString() {
		
		return this.getTitle() + ", " + this.getSaveMin() + " minutes and " + this.getSaveSec() + " seconds, completed at " + this.getFinished();
		
	}
}
