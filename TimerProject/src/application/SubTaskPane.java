package application;

import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.skin.ScrollPaneSkin;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseButton;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import java.io.File;
import java.util.Vector;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.io.IOException;
import java.util.Queue;



public class SubTaskPane extends BorderPane
{
	private Text title;
	private Text time;
	
	private int taskID;
	
	private boolean detectEvents = true;
	
	private TaskView parentDisplay;
	
	private SubTask task;


	
	public boolean selected = false;
	
	public SubTaskPane(SubTask s)
	{
		super();

		
		task = s;
		
		setMinHeight(35);
		setMinWidth(400);
		setStyle("-fx-background-color: rgb(128, 128, 128)");
		
		taskID = s.getId();
		
		title = new Text(s.getTitle());
		time = new Text(s.getWholeTime());
		
		
		setLeft(title);
		setRight(time);
		
		
		title.setFont(Font.font("Ariel", FontPosture.ITALIC, 20));
		time.setFont(Font.font("Ariel", FontPosture.ITALIC, 20));
		
		
		


		
		// Selection logic
		setOnMouseClicked(event -> {
			if(detectEvents)
			{
				// If user shift clicks on already selected item
				if(event.isShiftDown())
				{
					
					// If not many things are selected un-select current pane
					if(parentDisplay.singleSelection.get() == taskID)
					{
					
						if(parentDisplay.multiSelection.get() == -1)
						{
							parentDisplay.singleSelection.set(-1);
							selected = false;
						}
						
						// else if many things are selected un-select everything and then select this pane
						else
						{
							parentDisplay.multiSelection.set(-1);
							parentDisplay.singleSelection.set(-1);
							parentDisplay.singleSelection.set(taskID);						
							selected = true;
						}
					}
					
					// If nothing is selected then select thin pane
					else if(parentDisplay.singleSelection.get() == -1)
					{
						parentDisplay.singleSelection.set(taskID);
						selected = true;
					}
					
					// If many things are selected and usr clicks on the last selected item then un-select everything and then select
					// this pane
					else if(parentDisplay.multiSelection.get() == taskID)
					{
						parentDisplay.multiSelection.set(-1);
						parentDisplay.singleSelection.set(-1);
						parentDisplay.singleSelection.set(taskID);						
						selected = true;
					}
					// Else select all panes to be selected
					else
						parentDisplay.multiSelection.set(taskID);
				}
				// For normal clicks
				else
				{
					// If this is not selected then select this pane
					if(!selected)
					{
						// If many things were selected then un-select those
						if(parentDisplay.multiSelection.get() >= 0)
							parentDisplay.multiSelection.set(-1);
						
						parentDisplay.singleSelection.set(taskID);				
						selected = true;
					}
					// If this pane was selected
					else
					{
						// And if many things were selected un-select those and select this
						if(parentDisplay.multiSelection.get() >= 0)
						{
							parentDisplay.multiSelection.set(-1);
							parentDisplay.singleSelection.set(-1);
							parentDisplay.singleSelection.set(taskID);				
							selected = true;
						}
						// If only this pane was selected then un-select this pane
						else
						{
							parentDisplay.singleSelection.set(-1);
							selected = false;
						}
					}
				}
			}
			

		});
		
		// List of files that will hold files during drag and drop export
		Vector<File> fileList = new Vector<File>();
		


		// If drag is detected
		setOnDragDetected(event -> {
			if(detectEvents)
			{
				// If this pane is not selected or the selected list is empty do nothing
				if(parentDisplay.getSelected().isEmpty() || !selected)
					return;
				
				String idString = "Is Carrying value";
				

				
				
				// Create image overlay
				parentDisplay.togglePaneCreation();
				
				// Flag this as an internal drag since we are only re-arranging tasks or exporting
				parentDisplay.setInternalDrag(true);

				// Make a new dragboard with TransferMode.ANY so the system accepts file tansfers
				Dragboard dragboard = startDragAndDrop(TransferMode.ANY);
				ClipboardContent content = new ClipboardContent();
				
	
				
				String fileName = "";
				
				// If only 1 pane is selected than the file name if the user exports will be the title of the selected task pane
				if(parentDisplay.getSelected().size() == 1)
				{
					fileName = parentDisplay.getSelected().get(0).getTask().getTitle();
				}
				// Else it's just be "Untitled *number of tasks selected* tasks
				else if(parentDisplay.getSelected().size() > 1)
				{
					fileName = "Untitled " + Integer.toString(parentDisplay.getSelected().size()) + " tasks";
				}

				// Get a queue of teh selected tasks
				Queue<SubTask> selectedTasks = parentDisplay.convertToQueue(parentDisplay.getSelected());
				
				
				try
				{
					// Make a local csv file with fileName
					File f = new File(fileName + ".csv");
					
					
					f.createNewFile();
					
					// Export to the csv file
					parentDisplay.getCsvHandler().exportTask(selectedTasks, f);
					
					// Add file to file list
					fileList.add(f);
					
				}
				catch(IOException e)
				{
					e.printStackTrace();
				}
				
				
				// Put the fieList on the system clipboard
				content.putFiles(fileList);

				content.putString(idString);
				//dragboard.setDragView(d.getSnapShot());
				dragboard.setContent(content);
				
				
				
			}
		});
		
		
        setOnDragOver(event -> {
            if (event.getGestureSource() != this)
            {
            	// Accept drop only if this pane is NOT selected
            	if(!parentDisplay.getSelected().contains(this) && event.getDragboard().hasFiles())
            		event.acceptTransferModes(TransferMode.ANY);

            }

            //event.consume();
        });
        
        // Give user visual queues about when they can drop
        setOnDragEntered(event -> {
        	if (!parentDisplay.getSelected().contains(this))  
            {
                setOpacity(0.5);
            }
        });
        
        // Give user visual queues about when they can drop
        setOnDragExited(event -> {
            if (!parentDisplay.getSelected().contains(this)) {
                setOpacity(1);
            }
        });
        
        // When user drops
        setOnDragDropped(event -> {
        	// Delete image overlay
        	parentDisplay.togglePaneDeletion();
        	
        	// Set which SubTask the user dropped over
        	parentDisplay.setSwapPaneID(taskID);
        	
        	// Rearrange the tasks
        	parentDisplay.triggerDrag();
        });
        
        // After the drag delete any files still on the system clipboard
        setOnDragDone(event -> {
        	
        	event.getDragboard().getFiles().clear();
        	fileList.get(0).delete();
    		fileList.clear();
        	
        	
        });


		
	}
	
	public int getTaskID() { return taskID; }
	public SubTask getTask() { return task; }
	
	public boolean isDetectEvents() { return detectEvents; }
	public void setDetectEvents(boolean detect) { detectEvents = detect; }
	
	public void setParentDisplay(TaskView t)
	{
		parentDisplay = t;
	}
	
	public void highlight()
	{
		setStyle("-fx-background-color: rgb(137, 207, 240)");
	}
	
	public void unhighlight()
	{
		setStyle("-fx-background-color: rgb(128, 128, 128)");
	}
	


	public boolean equals(Object o)
	{
		if(o == null)
			return false;
		
		if(this == o)
			return true;
		
		if(o instanceof SubTaskPane)
		{
			SubTaskPane p = (SubTaskPane) o;
			
			if(p.getTaskID() != taskID)
				return false;
		}
		else
			return false;
		
		return true;
	}
}
