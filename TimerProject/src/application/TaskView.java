package application;

import java.util.ArrayList;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.VBox;
import java.util.LinkedList;
import java.util.Queue;


public class TaskView 
{
	private VBox tasks;
	private ArrayList<SubTaskPane> taskList;
	private ArrayList<SubTaskPane> selected;
	
	public IntegerProperty singleSelection;
	public IntegerProperty multiSelection;
	public BooleanProperty createDrag;
	
	private CsvHandler csvHandler;
	
	
	private int singleSelectedID;
	private int swapPaneID;
	private boolean internalDrag = false;
	
	public BooleanProperty deleteDragPane;
	public BooleanProperty createDragPane;


	
	public TaskView()
	{
		tasks = new VBox();
		tasks.setSpacing(5);
		tasks.setManaged(false);
		taskList = new ArrayList<SubTaskPane>();
		selected = new ArrayList<SubTaskPane>();
		
		// Property to signal task rearrangement
		createDrag = new SimpleBooleanProperty(false);
		
		// Property to signal single pane selection
		singleSelection = new SimpleIntegerProperty();
		
		// Property to signal multi pane selection
		multiSelection = new SimpleIntegerProperty();
		multiSelection.set(-1);
		
		
		// Properies to signal the creation and deletion of the drag image overlay
		deleteDragPane = new SimpleBooleanProperty(false);
		createDragPane = new SimpleBooleanProperty(false);

		
		tasks.setStyle("-fx-background-color: rgb(255, 255, 240)");
		
		// Single selection logic
		ChangeListener<Number> highlightSingle = new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) 
			{
				// Triggering clear of entire selected list
				if(singleSelection.get() == -1)
				{
					for(Node n : tasks.getChildren())
					{
						SubTaskPane currentTask = (SubTaskPane) n;
						
						if(selected.contains(currentTask))
						{
							currentTask.unhighlight();
							currentTask.selected = false;
							selected.remove(currentTask);
						}
					}
				}
				// Else remove all tasks from the selected array if they exist and only leave the task that has been selected
				else
				{
					// Loop through the taskView
					for(Node n : tasks.getChildren())
					{
						if(n instanceof SubTaskPane)
						{
							SubTaskPane currentTask = (SubTaskPane) n;
							// If the current pane is selected then put it on the selected list
							if(currentTask.getTaskID() == singleSelection.get())
							{
							
								currentTask.highlight();
								singleSelectedID = currentTask.getTaskID();
								selected.add(currentTask);
								
							}
							// Else remove it from the list if it is there
							else
							{
								if(selected.contains(currentTask))
								{
									currentTask.unhighlight();
									currentTask.selected = false;
									selected.remove(currentTask);
								}
							}
						}
					}
				}
			}
		};
		
		// Multiple selection logic
		ChangeListener<Number> highlightMulti = new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue)
			{
				// If it is not triggered to reset the selected list
				if(multiSelection.get() != -1)
				{
					int indexOne = indexOf(singleSelection.get());
					int indexTwo = indexOf(multiSelection.get());
					
					int minIndex;
					int maxIndex;
					
					if(indexOne >= 0 && indexTwo >= 0)
					{
						// Find the first and last index of the selected panes
						minIndex = indexOne < indexTwo ? indexOne : indexTwo;
						
						
						maxIndex = indexOne > indexTwo ? indexOne : indexTwo;
						
						// Unhihglight anything already selected
						for(int i = 0; i < selected.size(); ++i)
						{

							selected.get(i).unhighlight();							
							selected.get(i).selected = false;
							
						}
						// Remove everything from selected array
						selected.clear();
						
						
						for(int i = minIndex; i <= maxIndex; ++i)
						{
							//System.out.println("Adding: Task " + (i+1));
							Node n = tasks.getChildren().get(i);
							if(n instanceof SubTaskPane)
							{
								SubTaskPane sp = (SubTaskPane) n;
								
								sp.highlight();
								selected.add(sp);
								sp.selected = true;
							}
						}
					}
				}
				// Else remove everything from the selected list
				else
				{
					for(int i = 0; i < selected.size(); ++i)
					{

						selected.get(i).unhighlight();
						selected.get(i).selected = false;
					}
					
					selected.clear();
				}
			}
		};
		
		

		
		singleSelection.addListener(highlightSingle);
		multiSelection.addListener(highlightMulti);
	}
	
	
	public CsvHandler getCsvHandler() { return csvHandler; }
	public void setCsvHandler(CsvHandler handler) { csvHandler = handler; }
	
	// Triggering the creation of the image overlay
	public void togglePaneCreation()
	{
		if(createDragPane.get())
			createDragPane.set(false);
		else
			createDragPane.set(true);
	}
	
	// Triggering delection of the image overlay
	public void togglePaneDeletion()
	{
		if(deleteDragPane.get())
			deleteDragPane.set(false);
		else
			deleteDragPane.set(true);
	}

	
	public int getSwapPaneID() { return swapPaneID; }
	public void setSwapPaneID(int id) { swapPaneID = id; }
	
	public boolean isInternalDrag() { return internalDrag; }
	public void setInternalDrag(boolean drag) { internalDrag = drag; }
	

	
	public int indexOf(int id)
	{
		int index = 0;
		
		for(Node n : tasks.getChildren())
		{
			if(n instanceof SubTaskPane)
			{
				SubTaskPane sp = (SubTaskPane) n;
				
				if(sp.getTaskID() == id)
					return index;
			}
			
			++index;
		}
		
		return -1;
	}
	
	
	public VBox getView() { return tasks; }
	
	// Signals when tasks should be rearranged
	public void triggerDrag()
	{
		if(createDrag.get())
			createDrag.set(false);

		else
			createDrag.set(true);
	}
	
	public void addTask(SubTaskPane pane)
	{
		pane.setParentDisplay(this);		
		taskList.add(pane);	

		tasks.getChildren().add(taskList.get(taskList.size()-1));
		

	}
	
	
	

	
	public void removeTask(int id)
	{
		int index = 0;
		boolean found = false;
		
		SubTaskPane toRemove = null;
		
		for(int i = 0; i < taskList.size(); ++i)
		{
			if(id == taskList.get(i).getTaskID())
			{
				index = i;
				found = true;
				toRemove = taskList.get(i);
				break;
			}
		}
		
		
		if(toRemove.selected)
		{
			selected.remove(toRemove);
		}
			
		

		
		if(found)
		{
			tasks.getChildren().remove(index);
			taskList.remove(index);
		}
	}
	
	
	public void reOrderTasks(ArrayList<Integer> idList, int orderID)
	{
		ArrayList<SubTaskPane> newList = new ArrayList<SubTaskPane>();
		
		// Get the index of the first and last sub task pane in the idList.
		int smallestIndex = indexOf(idList.get(0));
		int largestIndex = indexOf(idList.get(idList.size()-1));
		
		// The index of the subtask pane that will either be pushed up or down by the selected tasks
		int indexOfOrder = indexOf(orderID);
		
		// If indexes are invalid then end the function
		if(smallestIndex == -1 || largestIndex == -1 || indexOfOrder == -1)
			return;
		
		// Eaxctly the same logic as reorder function in the CountDown class
		for(int i = 0; i < taskList.size(); ++i)
		{
			if(i == indexOfOrder)
			{
				if(indexOfOrder < smallestIndex)
				{
					for(int j = smallestIndex; j <= largestIndex; ++j)
					{
						newList.add(taskList.get(j));
					}
					
					newList.add(taskList.get(i));
				}
				else if(indexOfOrder > largestIndex)
				{
					newList.add(taskList.get(i));
					
					for(int j = smallestIndex; j <= largestIndex; ++j)
					{
						newList.add(taskList.get(j));
					}
				}
			}
			else if(i < smallestIndex || i > largestIndex)
			{
				newList.add(taskList.get(i));
			}
		}
		
		
		taskList = newList;
		
		tasks.getChildren().clear();
		
		// Update the display with the re-ordered tasks
		for(int i = 0; i < taskList.size(); ++i)
		{
			tasks.getChildren().add(taskList.get(i));
		}
	}
	
	
	public ArrayList<SubTaskPane> getSelected() { return selected; }
	
	// Convert an arrayList of subtask panes to a queue of subtasks
	public Queue<SubTask> convertToQueue(ArrayList<SubTaskPane> s)
	{
		
		ArrayList<SubTask> taskArray = new ArrayList<SubTask>();
		
		// Add the subtask of the subtask panes passed in to the taskArray SubTask arrayList
		for(SubTaskPane pane : s)
		{
			taskArray.add(pane.getTask());
		}
		
		Queue<SubTask> taskQueue = new LinkedList<SubTask>();
		
		// Add SubTasks form the taskAraay into a queue
		for(SubTask st : taskArray)
		{
			taskQueue.add(st);
		}
		
		return taskQueue;
	}


}
