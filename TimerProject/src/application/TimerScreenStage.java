package application;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import java.util.ArrayList;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.input.DragEvent;
import javafx.scene.input.TransferMode;
import javafx.stage.Popup;
import java.io.File;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;


// Attempted Fixed


public class TimerScreenStage {

	// Content of the screen
	private Stage stage;
	private Scene scene;
	private BorderPane content;
	private Button controlButton;
	private Button reset;
	private CsvHandler csvHandler;
	private TextField taskNameBox;
	private Spinner<Integer> minutePick;
	private Spinner<Integer> secondPick;
	private Button addButton;
	private TaskView taskView;
	private MenuBar menuBar;
	private StackPane listPane;
	private ScrollPane listScroller;
	private ImageView draggable;
	private Button logButton;
	private ListView<SubTask> log;
	

	public TimerScreenStage() {
		// TODO Auto-generated constructor stub
	}

	public TimerScreenStage(double sizeX, double sizeY, double positionX, double positionY) {
		// Formatting
		stage = new Stage();
		stage.setX(positionX);
		stage.setY(positionY);

		// Create the CountDown object
		CountDown cd = new CountDown();
		csvHandler = new CsvHandler();

		log = new ListView<SubTask>();
		
		// Create the menu
		Menu fileMenu = new Menu("File");
		
		// Create items
		MenuItem importOpt = new MenuItem("Import");
		MenuItem exportOpt = new MenuItem("Export All");
		
		// Add to fileMenu
		fileMenu.getItems().addAll(importOpt, exportOpt);
		
		// Create the menuBar
		menuBar = new MenuBar(fileMenu);
		menuBar.setTranslateX(0);
		menuBar.setTranslateY(0);
		
		// Labels for the title and time to be showed to the screen
		Label timerLabel = new Label(cd.timeString.get());
		Label titleLabel = new Label(cd.taskTitleString.toString());

		// Bind the labels to the text so that they also update when the text updates
		timerLabel.textProperty().bind(cd.timeString);
		titleLabel.textProperty().bind(cd.taskTitleString);

		// Formatting
		titleLabel.setFont(Font.font("Ariel", FontWeight.BOLD, 30));
		timerLabel.setFont(new Font(45));

		controlButton = new Button("PLAY");
		reset = new Button("RESET");
		logButton = new Button("Show Log");
		

		GridPane topButtons = new GridPane();

		topButtons.add(controlButton, 0, 0, 2, 1);
		topButtons.add(reset, 1, 0);
		topButtons.add(logButton, 2, 0);
		topButtons.setHgap(60);
		topButtons.setAlignment(Pos.CENTER);

		taskNameBox = new TextField();
		taskNameBox.setPromptText("Enter task name...");
		taskNameBox.setMinHeight(35);

		Label colonLabel = new Label(":");
		colonLabel.setFont(Font.font("Ariel", FontWeight.BOLD, 25));

		minutePick = new Spinner<Integer>(0, 59, 0);
		minutePick.setMinHeight(35);
		minutePick.setMaxWidth(65);
		minutePick.getEditor().setTextFormatter(new TextFormatter<>(c -> {
			if (c.getControlNewText().isEmpty() || c.getControlNewText().length() == 2)
				return c;

			String formatText = "0";

			if (c.getText().length() == 1) {
				formatText += Integer.toString(minutePick.getValue());
			} else
				formatText = Integer.toString(minutePick.getValue());

			c.setText(formatText);

			return c;
		}));

		secondPick = new Spinner<Integer>(0, 59, 0);
		secondPick.setMinHeight(35);
		secondPick.setMaxWidth(65);
		secondPick.getEditor().setTextFormatter(new TextFormatter<>(c -> {
			if (c.getControlNewText().isEmpty() || c.getControlNewText().length() == 2)
				return c;

			String formatText = "0";

			if (c.getText().length() == 1) {
				formatText += Integer.toString(secondPick.getValue());
			} else
				formatText = Integer.toString(secondPick.getValue());

			c.setText(formatText);

			return c;
		}));

		addButton = new Button("ADD");
		addButton.setMinHeight(35);
		addButton.setMinWidth(88);
		addButton.setFont(Font.font("Arial", FontWeight.BOLD, 15));
		addButton.setStyle("-fx-background-color: rgb(0, 112, 0)");

		GridPane taskInputPanel = new GridPane();

		taskInputPanel.add(minutePick, 0, 0);
		taskInputPanel.add(colonLabel, 1, 0);
		taskInputPanel.add(secondPick, 2, 0);
		taskInputPanel.add(addButton, 3, 0);

		taskInputPanel.setHgap(10);
		taskInputPanel.setAlignment(Pos.CENTER);


		
		taskView = new TaskView();

		// Adding a VBox inside a scroll pane so if too many panes get inserted into the vbox for it to dsplay the user can scroll
		// through
		listScroller = new ScrollPane();
		listScroller.setContent(taskView.getView());
		
		taskView.getView().fillWidthProperty().set(true);
		
		listPane = new StackPane();
		listPane.getChildren().add(listScroller);

		
		listPane.setPrefHeight(200);
		

		
		
		
		
		
		listScroller.setPrefHeight(200);
		
		listScroller.fitToHeightProperty().set(true);
		listScroller.fitToWidthProperty().set(true);

		content = new BorderPane();
		content.setTop(menuBar);
		GridPane grid = new GridPane();

		//grid.gridLinesVisibleProperty().set(true);
		
		
		String css = this.getClass().getResource("application.css").toExternalForm();

		// Adding labels to a gridpane

		grid.add(titleLabel, 0, 0, 2, 1);
		grid.add(timerLabel, 1, 1, 3, 1);
		grid.add(topButtons, 3, 0);

		grid.add(taskNameBox, 0, 14);

		grid.add(listPane, 0, 2, 5, 12);
		grid.add(taskInputPanel, 1, 14, 3, 1);
		

		// Create a Log stage, but don't show it until called by a button press
		LogScreenStage logWindow = new LogScreenStage(300, 300, 300, 300, log);

		grid.getColumnConstraints().add(new ColumnConstraints(200));
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setAlignment(Pos.TOP_CENTER);
		
		content.setCenter(grid);
		
		// Event Handler that checks when the window is closed and then shuts off the Timer
		EventHandler<WindowEvent> closeTimer = new EventHandler<WindowEvent>()
				{
					public void handle(WindowEvent e)
					{
						cd.stopTimer();
					}
				};
		
		// Event Handler that checks when the play or pause is clicked (might need to change logic if we add icons instead of words)
		EventHandler<ActionEvent> playPause = new EventHandler<ActionEvent>()
				{
					public void handle(ActionEvent e)
					{
						Button btn = (Button) e.getSource();
						
						String buttonName = btn.getText();
						
						switch(buttonName)
						{
						case "PLAY":
							controlButton.setText("PAUSE");
							cd.startCountDown();
							
							if(reset.isDisabled())
								reset.setDisable(false);
							break;
						case "PAUSE":
							controlButton.setText("PLAY");
							cd.stopTimer();							
							break;
						}
					}
				};
				
		// Resets current subtask
		EventHandler<ActionEvent> resetTask = new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				cd.resetCurrentTask();
			}
			

		};

		// Handles task importing via CSV
		EventHandler<ActionEvent> importCSV = new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				// Make ourselves a new FileChooser
				FileChooser selector = new FileChooser();
				// Assign it a title
				selector.setTitle("Open...");
				// Assign a filter to allow users to only select CSV files
				selector.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV files", "*.csv"));

				// Assign tasks through CsvHandler class
				try {
					csvHandler.assignTasks(cd, selector.showOpenDialog(stage), taskView);
				} catch (Exception importError) {
					// User probably closed the input window
					System.out.println(importError.toString());
				}
			}
		};

		// Handles task exporting via CSV
		EventHandler<ActionEvent> exportCSV = new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {

				// Make ourselves a new FileChooser
				FileChooser selector = new FileChooser();
				// Assign it a title
				selector.setTitle("Export To...");
				// Assign a filter to allow users to only select CSV files
				selector.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV files", "*.csv"));

				try {
					csvHandler.exportTask(cd.getTasks(), selector.showOpenDialog(stage));
				} catch (Exception exportError) {
					// User probably closed the input window
					exportError.printStackTrace();
				}

			}
		};
		
		EventHandler<ActionEvent> showLog = new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				

				logWindow.stage.show();
				

			}
		};
		
		
		log.setOnMouseClicked(new EventHandler<MouseEvent>() {

		    @Override
		    public void handle(MouseEvent click) {

		        if (click.getClickCount() == 2) {
		           SubTask currentItemSelected = log.getSelectionModel().getSelectedItem();
		           SubTask newCreatedTask = SubTask.createTask(currentItemSelected.getTitle(), currentItemSelected.getSaveMin(), currentItemSelected.getSaveSec());
		           newCreatedTask.setID(currentItemSelected.getId());
		           SubTaskPane logItem = new SubTaskPane(newCreatedTask);
		           cd.addTask(newCreatedTask);
		           taskView.addTask(logItem);
		        }
		    }
		});

		EventHandler<ActionEvent> addNewTask = new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				// If user has entered no task title then don't do anything
				if (!taskNameBox.getText().isEmpty()) {
					
					// Get value from the input fields
					String taskName = taskNameBox.getText();
					int minute = minutePick.getValue().intValue();
					int second = secondPick.getValue().intValue();
					
					// Clear the input fields
					taskNameBox.clear();
					minutePick.getValueFactory().setValue(0);
					secondPick.getValueFactory().setValue(0);

					SubTask newTask = SubTask.createTask(taskName, minute, second);

					// Add to the queue in the CountDown task and add to the visual display taskView
					taskView.addTask(new SubTaskPane(newTask));;
					cd.addTask(newTask);

				}
			}
		};
		
		// Update the task list when a task has finished
		ChangeListener<Boolean> updateTaskList = new ChangeListener<Boolean>() {
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (oldValue || newValue) {

					// Get the id from the recently removed task
					final int id = cd.getRecent().getId();

					// Add that task to the log
					Platform.runLater(new Runnable(){

						@Override
						public void run() {
							log.getItems().add(cd.getRecent());
							
						}
						
					});
					
					// Remove that taskPane fron the taskView
					Platform.runLater(() -> taskView.removeTask(id));
				}
			}
		};
		
		
		// Reorder tasks
		ChangeListener<Boolean> reorderTasks = new ChangeListener<Boolean>() {
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				boolean timerWasRunning = false;
				if (oldValue || newValue) {
					
					// Do nothing if nothing is selected
					if(!taskView.getSelected().isEmpty())
					{
						// If the tiemr is running then stop it
						if(cd.timerRunning())
						{
							cd.stopTimer();
							timerWasRunning = true;
						}
						
						
						ArrayList<Integer> idList = new ArrayList<Integer>();
						
						// Get the id's of the selected tasks
						for(int i = 0; i < taskView.getSelected().size(); ++i)
						{
							idList.add(taskView.getSelected().get(i).getTaskID());
						}
						
						// Reorder tasks in both the CountDown timer and the visual display
						cd.reOrderTasks(idList, taskView.getSwapPaneID());
						taskView.reOrderTasks(idList, taskView.getSwapPaneID());
						
						// unselect everything after reordering
						taskView.singleSelection.set(-1);
						taskView.multiSelection.set(-1);
						
						// If the timer was running then restart it
						if(timerWasRunning)
						{
							cd.startCountDown();
							timerWasRunning = false;
						}
						
					}
					
				}
			}
		};

		
		// Create image overlay
		ChangeListener<Boolean> createDragPane = new ChangeListener<Boolean>() {
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue)
			{
				if(draggable == null)
				{
					// Get a dragpane with the selected tasks
					DragPane d = new DragPane(taskView.getSelected());
					
					// Get image of that drag pane
					draggable = new ImageView(d.getSnapShot());
					
					// Make sure the image is not managed by any parent containers
					draggable.setManaged(false);
					draggable.setMouseTransparent(true);
					
					draggable.minWidth(400);
					
					// Make the image opaque and add it to the scene
					draggable.setOpacity(0.5);
					content.getChildren().add(draggable);
					content.setUserData(draggable);
					
					
					// Make the image follow the mouse when the user drags over the scene
					content.setOnDragOver(new EventHandler<DragEvent>() {
						public void handle(DragEvent e)
						{
							if(draggable != null)
							{
								
								draggable.relocate(e.getX(), e.getY());
								draggable.setOpacity(0.75);
							}
			
						}
					});
					
				}
			}
		};
		
		// Delete the drag pane
		ChangeListener<Boolean> deleteDragPane = new ChangeListener<Boolean>() {
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue)
			{
				if(draggable != null)
				{
					// Delete the image and all events connected to ie
					content.setOnMouseDragged(null);
					content.setOnDragOver(null);
					content.getChildren().remove(content.getUserData());
					content.setUserData(null);
					draggable = null;
					
				}
			}
		};
		
		// When the user drag mouse out of the scene and then drags back is create the image overlay
		content.setOnDragEntered(new EventHandler<DragEvent>() {
			public void handle(DragEvent e)
			{
				
				if(draggable != null)
				{
					
					draggable.relocate(e.getX(), e.getY());
				}
				else
				{
					taskView.togglePaneCreation();
				}

			}
		});
		
		
		// When user exits the scene while dragging delete the image overlay
		content.setOnDragExited(new EventHandler<DragEvent>()
		{


			public void handle(DragEvent e)
			{
				
				if(draggable != null)
					taskView.togglePaneDeletion();
			}
		});
		
		
		// When s drag is detected over the scene and the image overlay is not null reposition it to be under the mouse
		content.setOnDragDetected(new EventHandler<MouseEvent>()
		{
			public void handle(MouseEvent e)
			{
				
				if(draggable != null)
					draggable.relocate(e.getX(), e.getY());;
			}
		});

		// Make the list scroller accept external csv files that will be dragged and dropped
		listScroller.setOnDragOver(new EventHandler<DragEvent>() {
			public void handle(DragEvent e)
			{
				if(e.getGestureSource() != this && e.getDragboard().hasFiles() && !taskView.isInternalDrag())
				{
					e.acceptTransferModes(TransferMode.COPY);
				}
			}
		});
		
		// When csv file/s is dropped
		listScroller.setOnDragDropped(new EventHandler<DragEvent>() {
			public void handle(DragEvent e)
			{
				String error = "";
				if(e.getGestureSource() != this && e.getDragboard().hasFiles() && !taskView.isInternalDrag())
				{
					// Loop through the files
					for(int i = 0; i < e.getDragboard().getFiles().size(); ++i)
					{
						File file = e.getDragboard().getFiles().get(i);
						String fileExtension = file.getName().substring(file.getName().lastIndexOf("."), file.getName().length());
						
						// If the file extension is csv then add it to the program
						if(fileExtension.equals(".csv"))
							csvHandler.assignTasks(cd, file, taskView);
						// If not put the nae of the file in an error string
						else
						{
							error += file.getName() + " ";
						}
					}
						
					// If the error string is not emtpy then alert the user about the incorrect files they draged in
					if(!error.equals(""))
					{
						Alert warning = new Alert(AlertType.WARNING);
						
						warning.setHeaderText("Incorrect file type(s) detected");
						warning.setContentText("Only .csv files are accepted\n" + error + "are not acceptable files.");
						
						warning.show();
					}
					
				}
				
				taskView.setInternalDrag(false);
				
				e.consume();
			}
			
			
		});

		
		// Visually notify users when they are allowed to drop a file
		listScroller.setOnDragEntered(new EventHandler<DragEvent>() {
			public void handle(DragEvent e)
			{
				if(e.getGestureSource() != this && e.getDragboard().hasFiles() && !taskView.isInternalDrag())
				{
					taskView.getView().setStyle("-fx-background-color: rgb(255, 255, 204)");
				}
			}
		});
		
		listScroller.setOnDragExited(new EventHandler<DragEvent>() {
			public void handle(DragEvent e)
			{
				if(e.getGestureSource() != this && e.getDragboard().hasFiles() && !taskView.isInternalDrag())
				{
					taskView.getView().setStyle("-fx-background-color: rgb(255, 255, 240)");
				}
			}
		});

		cd.taskFinished.addListener(updateTaskList);
		

		taskView.createDrag.addListener(reorderTasks);
		taskView.createDragPane.addListener(createDragPane);
		taskView.deleteDragPane.addListener(deleteDragPane);
		taskView.setCsvHandler(csvHandler);


		stage.setOnCloseRequest(closeTimer);

		// Assign button events
		controlButton.setOnAction(playPause);
		reset.setOnAction(resetTask);
		addButton.setOnAction(addNewTask);
		logButton.setOnAction(showLog);
		
		importOpt.setOnAction(importCSV);
		exportOpt.setOnAction(exportCSV);

		reset.setDisable(true);
		
		addButton.setOnAction(addNewTask);
		
		
		
		
		scene = new Scene(content, sizeX, sizeY);
		scene.getStylesheets().add(css);
		stage.setScene(scene);
		stage.setTitle("Timer");
		stage.show();

	}

}
